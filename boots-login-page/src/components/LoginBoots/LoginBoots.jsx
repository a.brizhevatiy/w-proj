import React from 'react';
import {Button, Container, Form} from "react-bootstrap";
import {Formik} from "formik"
import * as Yup from "yup";
import axios from "axios"
import "./index.scss"

const LoginBoots = () => {
    const validSchema = Yup.object().shape({
        login: Yup.string().required("Login is required"),
        password: Yup.string().required("Password is required")
    })
    return (
        <Container className={"p-5 m-auto"} style={{
            width: "500px",
            boxShadow: "0 0 5px .1px #007bff",
            borderRadius: "10px",
        }} border={"success"}>
            <Formik
                initialValues={{login: "", password: ""}}
                validateOnBlur
                validationSchema={validSchema}
                onSubmit={(values, {setSubmitting, resetForm}) => {
                    setSubmitting(true);
                    resetForm();
                    const API = 'https://api.quartetsw.com/auth/oauth2/token'
                    const body = JSON.stringify({
                        username: values.login,
                        password: values.password,
                        grant_type: "",
                        client_id: ""
                    });
                    axios({
                        method: 'post',
                        url: API,
                        data: body
                    }).then(res => console.log(res))
                    setSubmitting(false);
                }}
            >
                {({
                      values,
                      errors,
                      touched,
                      handleChange,
                      handleBlur,
                      handleSubmit,
                      isSubmitting
                  }) => (
                    <Form onSubmit={handleSubmit} className="mx-auto">
                        <Form.Group controlId={"formBasicEmail"}>
                            <Form.Label><h4>Login</h4></Form.Label>
                            <Form.Control
                                type={"text"}
                                placeholder={"Login"}
                                name={"login"}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.login}
                                className={touched.login && errors.login ? "error" : null}/>
                            {touched.login && errors.login ? (
                                <div className="error-message">{errors.login}</div>
                            ) : null}
                        </Form.Group>

                        <Form.Group controlId={"formBasicPassword"}>
                            <Form.Label><h4>Password</h4></Form.Label>
                            <Form.Control
                                type={"password"}
                                placeholder={"Password"}
                                name={"password"}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.password}
                                className={touched.password && errors.password ? "error" : null}/>
                            {touched.password && errors.password ? (
                                <div className="error-message">{errors.password}</div>
                            ) : null}
                        </Form.Group>
                        <Button
                            variant={"primary"}
                            type={"submit"}
                            disabled={isSubmitting}
                        >Submit</Button>
                    </Form>
                )}
            </Formik>
        </Container>
    );
};

export default LoginBoots;
