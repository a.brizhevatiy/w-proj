import React, {useState} from 'react'
// import { Link } from 'react-router-dom'
import axios from "axios";
import {
    CButton,
    CCard,
    CCardBody,
    CCardGroup,
    CCol,
    CContainer,
    CForm,
    CInput,
    CInputGroup,
    CInputGroupPrepend,
    CInputGroupText,
    CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'


const Login = () => {
    const [log, setLog] = useState("")
    const [pass, setPass] = useState("")
    const API = 'https://api.quartetsw.com/auth/oauth2/token'
    const data = {
        username: log,
        password: pass,
        grant_type: "",
        client_id: ""
    };
    const headers = {}
    const submitHandler = (e) => {
        e.preventDefault();
        axios.post(API, data, {
            headers: headers,
        }).then(res => console.log(res))
        setLog("")
        setPass("")
    }

    return (
        <div className="c-app c-default-layout flex-row align-items-center">
            <CContainer>
                <CRow className="justify-content-center">
                    <CCol md="6">
                        <CCardGroup>
                            <CCard className="p-4">
                                <CCardBody>
                                    <CForm>
                                        <h1>Login</h1>
                                        <p className="text-muted">Sign In to your account</p>
                                        <CInputGroup className="mb-3">
                                            <CInputGroupPrepend>
                                                <CInputGroupText>
                                                    <CIcon name="cil-user"/>
                                                </CInputGroupText>
                                            </CInputGroupPrepend>
                                            <CInput type="text" placeholder="Username" autoComplete="username"
                                                    onChange={e => setLog(e.target.value)}/>
                                        </CInputGroup>
                                        <CInputGroup className="mb-4">
                                            <CInputGroupPrepend>
                                                <CInputGroupText>
                                                    <CIcon name="cil-lock-locked"/>
                                                </CInputGroupText>
                                            </CInputGroupPrepend>
                                            <CInput type="password" placeholder="Password"
                                                    autoComplete="current-password"
                                                    onChange={e => setPass(e.target.value)}/>
                                        </CInputGroup>
                                        <CRow>
                                            <CCol xs="6">
                                                <CButton type="submit" color="primary" className="px-4"
                                                         onClick={submitHandler}>Login</CButton>
                                            </CCol>
                                        </CRow>
                                    </CForm>
                                </CCardBody>
                            </CCard>
                        </CCardGroup>
                    </CCol>
                </CRow>
            </CContainer>
        </div>
    )
}
export default Login
